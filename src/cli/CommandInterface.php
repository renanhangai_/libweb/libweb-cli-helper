<?php
namespace libweb\cli;

/**
 * Basic interface for a command
 */
interface CommandInterface {
	// Setup the command
	public function setup($command, $container);

	// Run the command
	public function run($container);
}
