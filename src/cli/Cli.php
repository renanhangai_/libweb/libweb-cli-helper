<?php
namespace libweb\cli;

/**
 * Cli Helper
 */
abstract class Cli {
	// Setup the cli
	public function __construct() {
		$this->setup();
	}
	/**
	 * Setup the commands
	 */
	protected function setup() {
		// Construct the options
		$this->getOpt    = new \GetOpt\GetOpt();
		$this->container = new \Pimple\Container();
		$this->getOpt->addOption( \GetOpt\Option::create( 'h', 'help', \GetOpt\GetOpt::NO_ARGUMENT ) );
		
		// Setup the dependencies
		$this->container["getopt"] = $this->getOpt;
		$this->setupDependencies( $this->container );

		// Setup the commands
		$this->setupCommands();
	}
	/**
	 * Set commands dependencies
	 */
	protected function setupDependencies( $container ) {}
	/**
	 * Get a list of comamnds to run
	 */
	abstract function getCommands();
	/**
	 * Seta os comandos.
	 */
	protected function setupCommands() {
		$commandList = $this->getCommands();
		foreach ( $commandList as $commandClass ) {
			$commandName     = constant( $commandClass . '::COMMAND_NAME' );
			$commandInstance = new $commandClass();

			// Add the command to the list
			$getOptCommand   = \GetOpt\Command::create( $commandName, function () use ($commandInstance) {
				return $commandInstance->run( $this->container );
			});
			// Add the description
			if ( defined( $commandClass . '::COMMAND_DESCRIPTION' ) ) {
				$getOptCommand->setDescription( constant( $commandClass . '::COMMAND_DESCRIPTION' ) );
			}
			// Setup the command
			$options = $commandInstance->setup( $getOptCommand, $this->container );
			if ( is_array($options) ) {
				$operands = null;
				if ( array_key_exists( '_', $options ) ) {
					$operands = self::operands( $options['_'] );
					unset( $options['_'] );
				}
				$options = self::options( $options );
				foreach( $options as $opt )
					$getOptCommand->addOption( $opt );
				if ( $operands )
					$getOptCommand->addOperands( $operands );
			}
			$this->getOpt->addCommand( $getOptCommand );

			// Set the command container
			$this->container[$commandClass] = $commandInstance;
		}
	}

	/**
	 * Roda o programa.
	 */
	public function run() {
		// process arguments and catch user errors
		try {
			try {
				$this->getOpt->process();
			} catch (\GetOpt\ArgumentException\Missing $exception) {
				// catch missing exceptions if help is requested
				if (!$this->getOpt->getOption('help')) {
					throw $exception;
				}
			}
		} catch (\GetOpt\ArgumentException $exception) {
			file_put_contents('php://stderr', $exception->getMessage() . PHP_EOL);
			echo PHP_EOL . $this->getOpt->getHelpText();
			exit;
		}

		$command = $this->getOpt->getCommand();
		if (!$command || $this->getOpt->getOption('help')) {
			echo $this->getOpt->getHelpText();
			exit;
		}
		$this->container['options'] = $this->getOpt->getOptions();
		call_user_func( $command->getHandler(), $this->getOpt );
	}
	/**
	 * Main function to run
	 */
	public static function main() {
		$cli = new static();
		$cli->run();
	}

	// Helper methods
	/**
	 * Normalize an option from a string
	 */
	public static function option($option) {
		if ( !$option )
			return null;
		if ( is_string($option) ) {
			$isMatch = preg_match( '/^\s*(?:(\w)\:)?(\w+)(?:\s+(\<.*?\>|\[.*\]))?(\s+.*?)?$/', trim($option), $matches );
			if ( !$isMatch ) {
				throw new \InvalidArgumentException( 'Option is not valid' );
			}
			$shortName = $matches[1] ?: null;
			$longName = $matches[2];
			$argType = \GetOpt\GetOpt::NO_ARGUMENT;
			$description = trim($matches[4] ?? '');
			if ( @$matches[3] ) {
				$argMatchChar = $matches[3][0];
				if ( '<' === $argMatchChar ) {
					$argType = \GetOpt\GetOpt::REQUIRED_ARGUMENT;
				} elseif ( '[' === $argMatchChar ) {
					$argType = \GetOpt\GetOpt::OPTIONAL_ARGUMENT;
				}
			}
			return \GetOpt\Option::create( $shortName, $longName, $argType )
				->setDescription( $description );
		} elseif ( $option instanceof \GetOpt\Option ) {
			return $option;
		}
		throw new \InvalidArgumentException( 'Option is not valid' );
	}
	/// Normalize an array of options
	public static function options($optionMap) {
		return array_filter( array_map(function ($option) { return self::option( $option ); }, $optionMap) );
	}
	/// Operand
	public static function operand($operand) {
		if ( !$operand )
			return null;
		if ( is_string($operand) ) {
			$operand = trim($operand);
			if ( $operand[0] === '<' && substr( $operand, -1 ) === '>' )
				$operandType = \GetOpt\Operand::REQUIRED;
			else if ( $operand[0] === '[' && substr( $operand, -1 ) === ']' )
				$operandType = \GetOpt\Operand::OPTIONAL;
			else 
				throw new \InvalidArgumentException( 'Operand must be [name] or <name>' );

			$operandContent = substr( $operand, 1, strlen( $operand ) - 2 );
			$isMatch = preg_match( '/^(\w+)(\.\.\.)?$/', trim($operandContent), $matches );
			if ( !$isMatch ) {
				throw new \InvalidArgumentException( 'Operand name is not valid, must be either name or name...' );
			}
			$operandName = $matches[1];
			if ( @$matches[2] ) 
				$operandType = $operandType | \GetOpt\Operand::MULTIPLE;
			return \GetOpt\Operand::create( $operandName, $operandType );
		} elseif ( $operand instanceof \GetOpt\Operand ) {
			return $operand;
		}
		throw new \InvalidArgumentException( 'Operand is not valid' );
	}
	/// Operand
	public static function operands($operands) {
		if ( !$operands )
			return [];
		if ( is_string($operands) )
			$operands = explode( " ", $operands );
		return array_filter( array_map(function ($operand) { return self::operand( $operand ); }, $operands) );
	}

	private $getOpt;
	private $container;
}
