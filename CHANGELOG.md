# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/renanhangai_/libweb/libweb-cli-helper/compare/v0.1.0...v0.2.0) (2018-11-30)


### Features

* New operand setup methods ([ef7d29f](https://gitlab.com/renanhangai_/libweb/libweb-cli-helper/commit/ef7d29f))



<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/renanhangai_/libweb/libweb-cli-helper/compare/v0.0.1...v0.1.0) (2018-11-29)


### Features

* Added basic interface as a guide to commands ([578798e](https://gitlab.com/renanhangai_/libweb/libweb-cli-helper/commit/578798e))
* Added option helper methods to generate better options using short syntax ([a09b66d](https://gitlab.com/renanhangai_/libweb/libweb-cli-helper/commit/a09b66d))



<a name="0.0.1"></a>
## 0.0.1 (2018-11-29)
